﻿import { StunService } from 'collab.io-stun-service';
import { RelayService } from 'collab.io-relay-service';
import { HttpService } from 'collab.io-http-service';

import socketio = require('socket.io');
import express = require('express');
import path = require('path');

var app = express();
var http = app.listen(process.env.port || 1337);
let io = socketio(http);
app.use(express.static(path.join(process.cwd(), 'node_modules/collab.io-editor/dist')));
app.use('/assets', express.static(path.join(process.cwd(), 'node_modules/collab.io-server/assets')));

app.get('/', (req, res) => {
    res.sendFile(path.join(process.cwd(), 'node_modules/collab.io-editor/dist/main.html'));
});
app.get('/editor/*', (req, res) => {
    res.sendFile(path.join(process.cwd(), 'node_modules/collab.io-editor/dist/editor.html'));
});

new StunService(io);
new RelayService(io);
new HttpService(app);